import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('dashboard', {path: '/'}, function(){
    this.route('dashboard', {path: '/dashboard'});
  });
  this.route('restaurants', { path: '/restaurants' }, function() {
    /*this.route('restaurants', { path: '/restaurants' });
    this.route('edit', { path: '/:restaurant_id/edit' });*/
  });
  this.route('orders', { path: '/orders' }, function() {
    /*this.route('restaurants', { path: '/restaurants' });
    this.route('edit', { path: '/:restaurant_id/edit' });*/
  });
  this.route('users', { path: '/users' }, function() {
    /*this.route('index', { path: '/' });
    this.route('edit', { path: '/:restaurant_id/edit' });*/
  });
  this.route('login', { path: '/login' }, function() {
    /*this.route('index', { path: '/' });
    this.route('edit', { path: '/:restaurant_id/edit' });*/
  });
});

export default Router;
